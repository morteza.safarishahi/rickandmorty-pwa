## Technical overview
We have been using React, NextJs, ReduxToolkit,Axios, Typescript and ANTD in this project.

minimum compatible node version: 14

## Contact information
Project started at 7 February 2021, created, maintained  and developed by [Morteza SafariShahi](mailto:safarishahim@gmail.com).
