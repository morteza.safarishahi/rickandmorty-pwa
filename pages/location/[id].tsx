import wrapper from '@/Module/Store/Model/wrapper';
import { getLocationAction, locationRemove, locationSelectById } from '@/Module/RickAndMorty/Store/locationSlice';
import { FC, useEffect } from 'react';
import useAppDispatch from '@/Module/Store/Hook/useAppDispatch';
import LocationView from '@/Module/RickAndMorty/Component/LocationView/LocationView';
import useAppSelector from '@/Module/Store/Hook/useAppSelector';
import Head from 'next/head';

interface ILocationPageProps{
  locationId: number;
}

const LocationPage: FC<ILocationPageProps> = ({ locationId }) => {
  const dispatch = useAppDispatch();
  const locationName = useAppSelector((state) => locationSelectById(state, locationId)?.name);

  useEffect(() => () => {
    dispatch(locationRemove(locationId));
  }, []);

  return (
    <>
      <Head>
        <title>
          {`${locationName} | Rick And Morty`}
        </title>
      </Head>

      <LocationView locationId={locationId} />
    </>
  );
};

export const getServerSideProps = wrapper.getServerSideProps((store) => async ({ params }) => {
  const locationId = Number(params?.id);
  if (locationId) {
    await store.dispatch(getLocationAction(locationId));

    return {
      props: {
        locationId,
      },
    };
  }

  return {
    notFound: true,
  };
});

export default LocationPage;
