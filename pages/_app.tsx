import { AppProps } from 'next/app';
import { ConfigProvider } from 'antd';
import en_US from 'antd/lib/locale/en_US';
import ApplicationLayout from '@/Module/Application/Component/ApplicationLayout/ApplicationLayout';
import ApplicationHead from '@/Module/Application/Component/ApplicationHead/ApplicationHead';
import wrapper from '@/Module/Store/Model/wrapper';
import '@/Resource/Css/main.sass';
import Cookies from 'universal-cookie';
import { setFavoriteId } from '@/Module/RickAndMorty/Store/userSlice';

const App = ({ Component, pageProps }: AppProps) => (
  <ConfigProvider
    direction="ltr"
    locale={en_US}
    componentSize="middle"
  >
    <ApplicationHead />
    <ApplicationLayout>
      <Component {...pageProps} />
    </ApplicationLayout>
  </ConfigProvider>
);
App.getInitialProps = wrapper.getInitialAppProps((store) => async ({ Component, ctx }) => {
  const cookies = new Cookies(ctx.req?.headers?.cookie);

  await store.dispatch(setFavoriteId(Number(cookies.get('favoriteId') || 0)));

  const pageProps = Component.getInitialProps
    ? await Component.getInitialProps({
      ...ctx,
      store,
    })
    : {};

  return {
    pageProps,
    initialReduxState: store.getState(),
  };
});

export default wrapper.withRedux(App);
