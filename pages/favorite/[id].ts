import { NextPage } from 'next';
import wrapper from '@/Module/Store/Model/wrapper';
import { serialize } from 'cookie';
import { setFavoriteId } from '@/Module/RickAndMorty/Store/userSlice';
import { Router } from 'next/router';
import isBrowser from '@/Module/Application/Helper/isBrowser';

const FavoritePage: NextPage = () => null;

export const getServerSideProps = wrapper.getServerSideProps((store) => async ({ params, res, query }) => {
  const redirectUrl = query?.redirect ? decodeURIComponent(query.redirect as string) : '/';

  if (params && params?.id) {
    const favoriteArr = String(params.id).split('-');
    const favoriteId = Number(favoriteArr[1]);
    await store.dispatch(setFavoriteId(favoriteId));

    res.setHeader('Set-Cookie', serialize('favoriteId', String(favoriteId), { path: '/' }));

    if (!isBrowser()) {
      res.writeHead(302, { Location: redirectUrl });
      res.end();
      return {
        props: {
          favoriteId,
        },
      };
    }

    // @ts-ignore
    Router?.replace(redirectUrl);
  }

  return {
    redirect: {
      destination: redirectUrl,
      permanent: true,
    },
  };
});

export default FavoritePage;
