import { FC } from 'react';
import axiosInstance from '@/Module/Http/Helper/axiosInstance';
import { AxiosResponse } from 'axios';
import { ICharacter } from '@/Module/RickAndMorty/Model/DataModel/ICharacter';
import { GetStaticPaths, GetStaticProps } from 'next';
import CharacterView from '@/Module/RickAndMorty/Component/CharacterView/CharacterView';
import { useRouter } from 'next/router';
import { ILocation } from '@/Module/RickAndMorty/Model/DataModel/ILocation';
import RICK_AND_MORTY_API from '@/Module/RickAndMorty/Constant/RICK_AND_MORTY_API';
import { IEpisode } from '@/Module/RickAndMorty/Model/DataModel/IEpisode';
import { IInfo } from '@/Module/RickAndMorty/Model/ApiModel/IInfo';
import Head from 'next/head';

interface ICharacterPageProps{
  character: ICharacter;
  episodes: IEpisode[];
  origin?: ILocation,
  location?: ILocation,
}

const CharacterPage: FC<ICharacterPageProps> = ({ character, episodes, origin, location }) => {
  const router = useRouter();

  if (router.isFallback) {
    return <div>Loading...</div>;
  }

  return (
    <>
      <Head>
        <title>
          {`${character.name} | Rick And Morty`}
        </title>
      </Head>

      <CharacterView
        character={character}
        episodes={episodes}
        origin={origin}
        location={location}
      />
    </>
  );
};

CharacterPage.defaultProps = {
  origin: undefined,
  location: undefined,
};

// @ts-ignore
export const getStaticPaths: GetStaticPaths = async () => {
  const { data } = await axiosInstance.get<any, AxiosResponse<IInfo<ICharacter>>>(RICK_AND_MORTY_API.CHARACTERS);
  const totalPages = data.info?.pages || 0;
  const paths = data.results?.map((item) => ({ params: { id: item.id.toString() } })) || [];

  const promises: Promise<any>[] = [];

  for (let page = 2; page <= totalPages; page++) {
    promises.push(
      axiosInstance.get<any, AxiosResponse<IInfo<ICharacter>>>(`${RICK_AND_MORTY_API.CHARACTERS}?page=${page}`).then(({ data: { results = [] } }) => {
        paths.push(...results.map((item) => ({ params: { id: item.id.toString() } })));
      }),
    );
  }

  await Promise.allSettled(promises);

  return { paths, fallback: 'blocking' };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const { data: character } = await axiosInstance.get<any, AxiosResponse<ICharacter>>(`${RICK_AND_MORTY_API.CHARACTERS}/${params?.id}`);
  const props: ICharacterPageProps = {
    character,
    episodes: [],
  };

  const promises: Promise<any>[] = [];

  if (character.origin?.url) {
    promises.push(
      axiosInstance.get<any, AxiosResponse<ILocation>>(character.origin.url).then(({ data: origin }) => {
        props.origin = origin;
      }),
    );
  }

  if (character.location?.url) {
    promises.push(
      axiosInstance.get<any, AxiosResponse<ILocation>>(character.location.url).then(({ data: location }) => {
        props.location = location;
      }),
    );
  }

  character.episode.forEach((episodeUrl) => {
    promises.push(
      axiosInstance.get<any, AxiosResponse<IEpisode>>(episodeUrl).then(({ data: episode }) => {
        props.episodes.push(episode);
      }),
    );
  });

  await Promise.allSettled(promises);

  return { props, revalidate: 10 * 60 };
};

export default CharacterPage;
