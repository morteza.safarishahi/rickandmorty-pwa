import { NextPage } from 'next';
import Head from 'next/head';
import CharacterList from '@/Module/RickAndMorty/Component/CharacterList/CharacterList';
import LocationList from '@/Module/RickAndMorty/Component/LocationList/LocationList';

const HomePage: NextPage = () => (
  <>
    <Head>
      <title>Home | Rick And Morty</title>
    </Head>

    <CharacterList />
    <LocationList />
  </>
);

export default HomePage;
