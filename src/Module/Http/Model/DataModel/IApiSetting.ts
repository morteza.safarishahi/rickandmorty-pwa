export interface IApiSetting {
  HOST_URL: string;
  BASE_URL: string;
}
