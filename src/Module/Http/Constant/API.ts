import { IENV } from '@/Module/Http/Model/DataModel/IENV';
import { IApiSetting } from '@/Module/Http/Model/DataModel/IApiSetting';
import API_SETTING from '@/Module/Http/Constant/API_SETTING';

const buildEnv: IENV = process.env.BUILD_ENV as IENV || 'DEVELOP';
const requestSetting: IApiSetting = API_SETTING[buildEnv];

const API_BASE = process.env.BASE_URL || requestSetting.BASE_URL;
export const HOST_URL = process.env.BASE_URL || requestSetting.HOST_URL;

export default API_BASE;
