import { IENV } from '@/Module/Http/Model/DataModel/IENV';
import { IApiSetting } from '@/Module/Http/Model/DataModel/IApiSetting';
import API_SETTING_BETA from '@/Module/Http/Constant/API_SETTING_BETA';
import API_SETTING_PRODUCTION from '@/Module/Http/Constant/API_SETTING_PRODUCTIONA';

const API_SETTING: Record<IENV, IApiSetting> = {
  BETA: API_SETTING_BETA,
  PRODUCTION: API_SETTING_PRODUCTION,
  DEVELOP: API_SETTING_BETA,
};

export default API_SETTING;
