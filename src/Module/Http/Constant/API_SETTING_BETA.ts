import { IApiSetting } from '@/Module/Http/Model/DataModel/IApiSetting';

const API_SETTING_BETA: IApiSetting = {
  HOST_URL: 'http://localhost:3000',
  BASE_URL: 'https://rickandmortyapi.com/api',
};

export default API_SETTING_BETA;
