import axios from 'axios';
import API_BASE from '@/Module/Http/Constant/API';

const axiosInstance = axios.create({
  baseURL: API_BASE,
});

// If you need to test, you can uncomment
// axiosLoggerInit(axiosInstance);

export default axiosInstance;
