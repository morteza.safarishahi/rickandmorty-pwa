import { AxiosError, AxiosInstance } from 'axios';
import { errorLogger, requestLogger } from 'axios-logger';
import isBrowser from '@/Module/Application/Helper/isBrowser';

const axiosErrorLogger = (error: AxiosError<any>) => {
  if (isBrowser()) return error;
  return errorLogger(error);
};

const axiosLoggerInit = (instance: AxiosInstance) => {
  instance.interceptors.request.use((request) => {
    if (isBrowser()) return request;
    return requestLogger(request);
  }, axiosErrorLogger);

  instance.interceptors.response.use((response) => {
    if (isBrowser()) return response;
    return requestLogger(response);
  }, axiosErrorLogger);
};

export default axiosLoggerInit;
