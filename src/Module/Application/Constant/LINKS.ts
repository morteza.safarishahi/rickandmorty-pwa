const LINKS = {
  HOME: '/',
  CHARACTER: '/character/[id]',
  LOCATION: '/location/[id]',
  FAVORITE: '/favorite/[id]',
};

export default LINKS;
