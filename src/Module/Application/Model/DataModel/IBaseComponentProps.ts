import { CSSProperties } from 'react';

export interface IBaseComponentProps {
  className?: string;
  id?: string;
  style?: CSSProperties;
  testName?: string;
}
