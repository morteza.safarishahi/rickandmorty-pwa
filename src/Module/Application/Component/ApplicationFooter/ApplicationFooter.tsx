import { FC } from 'react';
import { Button, Layout } from 'antd';
import s from './ApplicationFooter.module.scss';

const { Footer } = Layout;

const ApplicationFooter: FC = () => (
  <Footer className={s.footer}>
    Made by
    <Button
      href="mailto:safarishahim@gmail.com"
      type="link"
    >
      Morteza SafariShahi
    </Button>
  </Footer>
);

export default ApplicationFooter;
