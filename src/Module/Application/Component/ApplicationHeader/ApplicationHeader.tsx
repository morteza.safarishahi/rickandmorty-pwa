import { FC, useEffect, useState } from 'react';
import { Button, Layout } from 'antd';
import useAppSelector from '@/Module/Store/Hook/useAppSelector';
import { DeleteOutlined, LeftOutlined } from '@ant-design/icons';
import { useRouter } from 'next/router';
import { ICharacter } from '@/Module/RickAndMorty/Model/DataModel/ICharacter';
import axiosInstance from '@/Module/Http/Helper/axiosInstance';
import RICK_AND_MORTY_API from '@/Module/RickAndMorty/Constant/RICK_AND_MORTY_API';
import { AxiosResponse } from 'axios';
import LINKS from '@/Module/Application/Constant/LINKS';
import Cookies from 'universal-cookie';
import useAppDispatch from '@/Module/Store/Hook/useAppDispatch';
import { setFavoriteId } from '@/Module/RickAndMorty/Store/userSlice';
import s from './ApplicationHeader.module.scss';

const { Header } = Layout;

const ApplicationHeader: FC = () => {
  const router = useRouter();
  const dispatch = useAppDispatch();
  const favoriteId = useAppSelector((state) => state.user.favoriteId);
  const [favorite, setFavorite] = useState<ICharacter>();

  const onReset = () => {
    router.replace({
      pathname: LINKS.FAVORITE,
      query: {
        id: 'character-0',
        redirect: encodeURIComponent(document.location.pathname),
      },
    });
  };

  const onBack = () => {
    router.back();
  };

  const updateFavorite = () => {
    axiosInstance.get<any, AxiosResponse<ICharacter>>(`${RICK_AND_MORTY_API.CHARACTERS}/${favoriteId}`).then(({ data }) => {
      setFavorite(data);
    });
  };

  useEffect(() => {
    if (favoriteId) {
      updateFavorite();
      return;
    }

    setFavorite(undefined);
  }, [favoriteId]);

  useEffect(() => {
    const cookies = new Cookies();
    const cookieFavoriteId = Number(cookies.get('favoriteId') || 0);
    if (cookieFavoriteId !== favoriteId) {
      dispatch(setFavoriteId(cookieFavoriteId));
    }
  }, [router.isFallback]);

  return (
    <Header className={s.header}>
      {router.pathname !== '/' && (
        <Button
          className="m-r-16 colorWhite"
          type="text"
          title="back"
          icon={<LeftOutlined />}
          onClick={onBack}
        />
      )}
      {`Hello, ${favorite?.name || 'Guest'}`}
      {favorite && (
      <Button
        className="m-l-16"
        type="text"
        danger
        title="reset favorite"
        icon={<DeleteOutlined />}
        onClick={onReset}
      />
      ) }
    </Header>
  );
};

export default ApplicationHeader;
