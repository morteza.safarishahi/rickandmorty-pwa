import { FC } from 'react';
import { Layout } from 'antd';
import MBWrapper from '@/DesignSystem/MBWrapper/MBWrapper';

const { Content } = Layout;

const ApplicationContent: FC = ({ children }) => (
  <Content>
    <MBWrapper>{children}</MBWrapper>
  </Content>
);

export default ApplicationContent;
