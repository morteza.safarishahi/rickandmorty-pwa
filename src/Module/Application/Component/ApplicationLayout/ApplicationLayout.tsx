import { FC } from 'react';
import { Layout } from 'antd';
import ApplicationFooter from '@/Module/Application/Component/ApplicationFooter/ApplicationFooter';
import ApplicationContent from '@/Module/Application/Component/ApplicationContent/ApplicationContent';
import dynamic from 'next/dynamic';
import NextNProgress from 'nextjs-progressbar';
import s from './ApplicationLayout.module.scss';

const ApplicationHeader = dynamic(() => import('@/Module/Application/Component/ApplicationHeader/ApplicationHeader'), {
  ssr: false,
});

const ApplicationLayout: FC = ({ children }) => (
  <Layout className={s.layout}>
    <NextNProgress />
    <ApplicationHeader />
    <ApplicationContent>{children}</ApplicationContent>
    <ApplicationFooter />
  </Layout>
);

export default ApplicationLayout;
