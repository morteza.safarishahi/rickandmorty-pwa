import { FC } from 'react';
import s from './MBWrapper.module.scss';

const MBWrapper: FC = ({ children }) => <div className={s.wrapper}>{children}</div>;

export default MBWrapper;
