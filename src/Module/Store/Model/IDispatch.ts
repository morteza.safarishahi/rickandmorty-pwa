import { IAppStore } from '@/Module/Store/Model/IAppStore';

export type IDispatch = IAppStore['dispatch'];
