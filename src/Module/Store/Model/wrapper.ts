import { createWrapper } from 'next-redux-wrapper';
import { IAppStore } from '@/Module/Store/Model/IAppStore';
import makeStore from '@/Module/Store/Model/makeStore';

const wrapper = createWrapper<IAppStore>(makeStore);

export default wrapper;
