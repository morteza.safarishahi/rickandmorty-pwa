import { configureStore } from '@reduxjs/toolkit';
import rootReducer from '@/Module/Store/Model/rootReducer';
import { locationApi } from '@/Module/RickAndMorty/Store/locationApi';
import { episodeApi } from '@/Module/RickAndMorty/Store/episodeApi';
import { charactersApi } from '@/Module/RickAndMorty/Store/characterApi';

const makeStore = () => configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware()
    .concat(charactersApi.middleware)
    .concat(locationApi.middleware)
    .concat(episodeApi.middleware),
  devTools: true,
});

export default makeStore;
