/* eslint-disable no-param-reassign */
import { combineReducers } from '@reduxjs/toolkit';
import { HYDRATE } from 'next-redux-wrapper';
import { charactersApi } from '@/Module/RickAndMorty/Store/characterApi';
import { locationApi } from '@/Module/RickAndMorty/Store/locationApi';
import { episodeApi } from '@/Module/RickAndMorty/Store/episodeApi';
import userSlice from '@/Module/RickAndMorty/Store/userSlice';
import locationSlice from '@/Module/RickAndMorty/Store/locationSlice';

const appReducer = combineReducers({
  [userSlice.name]: userSlice.reducer,
  [locationSlice.name]: locationSlice.reducer,
  [charactersApi.reducerPath]: charactersApi.reducer,
  [locationApi.reducerPath]: locationApi.reducer,
  [episodeApi.reducerPath]: episodeApi.reducer,
});

type IAppReducer = typeof appReducer;

const rootReducer:IAppReducer = (state, action) => {
  if (action.type === HYDRATE) {
    state = {
      ...state,
      ...action.payload,
    };
  }

  return appReducer(state, action);
};

export default rootReducer;
