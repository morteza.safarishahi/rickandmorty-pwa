import makeStore from '@/Module/Store/Model/makeStore';

export type IAppStore = ReturnType<typeof makeStore>;
