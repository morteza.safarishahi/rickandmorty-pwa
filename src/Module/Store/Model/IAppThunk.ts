import { Action, ThunkAction } from '@reduxjs/toolkit';
import { IState } from '@/Module/Store/Model/IState';

export type IAppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  IState,
  unknown,
  Action<string>
  >;
