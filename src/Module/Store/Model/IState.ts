import { IAppStore } from '@/Module/Store/Model/IAppStore';

export type IState = ReturnType<IAppStore['getState']>;
