import { TypedUseSelectorHook, useSelector } from 'react-redux';
import { IState } from '@/Module/Store/Model/IState';

const useAppSelector: TypedUseSelectorHook<IState> = useSelector;

export default useAppSelector;
