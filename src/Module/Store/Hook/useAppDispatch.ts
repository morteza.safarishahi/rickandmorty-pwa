import { useDispatch } from 'react-redux';
import { IDispatch } from '@/Module/Store/Model/IDispatch';

const useAppDispatch = () => useDispatch<IDispatch>();

export default useAppDispatch;
