import { createSlice, PayloadAction } from '@reduxjs/toolkit';

const userSlice = createSlice({
  name: 'user',
  initialState: {
    favoriteId: 0,
  },
  reducers: {
    setFavoriteId: (state, { payload }: PayloadAction<number>) => {
      state.favoriteId = payload;
    },
  },
});

export const { setFavoriteId } = userSlice.actions;

export default userSlice;
