import { createAsyncThunk, createEntityAdapter, createSlice, PayloadAction } from '@reduxjs/toolkit';
import axiosInstance from '@/Module/Http/Helper/axiosInstance';
import RICK_AND_MORTY_API from '@/Module/RickAndMorty/Constant/RICK_AND_MORTY_API';
import { ILocation } from '@/Module/RickAndMorty/Model/DataModel/ILocation';
import { IState } from '@/Module/Store/Model/IState';

const locationAdapter = createEntityAdapter<ILocation>({
  selectId: (model) => model.id,
});

export const getLocationAction = createAsyncThunk<ILocation, number>(
  'location/updateLocation',
  (locationId) => axiosInstance.get(`${RICK_AND_MORTY_API.LOCATIONS}/${locationId}`).then(({ data }) => data),
);

const locationSlice = createSlice({
  name: 'location',
  initialState: locationAdapter.getInitialState({
    isLoading: false,
  }),
  reducers: {
    locationRemove: (state, { payload }: PayloadAction<number>) => {
      locationAdapter.removeOne(state, payload);
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getLocationAction.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getLocationAction.rejected, (state) => {
        state.isLoading = false;
      })
      .addCase(getLocationAction.fulfilled, (state, { payload }) => {
        state.isLoading = false;
        locationAdapter.setOne(state, payload);
      });
  },
});

export const {
  selectById: locationSelectById,
} = locationAdapter.getSelectors<IState>((state) => state.location);

export const {
  locationRemove,
} = locationSlice.actions;

export default locationSlice;
