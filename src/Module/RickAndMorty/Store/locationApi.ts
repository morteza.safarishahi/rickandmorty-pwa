import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/dist/query/react';
import API_BASE from '@/Module/Http/Constant/API';
import { IInfo } from '@/Module/RickAndMorty/Model/ApiModel/IInfo';
import RICK_AND_MORTY_API from '@/Module/RickAndMorty/Constant/RICK_AND_MORTY_API';
import { ILocation } from '@/Module/RickAndMorty/Model/DataModel/ILocation';
import { ILocationListRequest } from '@/Module/RickAndMorty/Model/ApiModel/ILocationListRequest';

export const locationApi = createApi({
  reducerPath: 'locationApi',
  baseQuery: fetchBaseQuery({ baseUrl: API_BASE }),
  endpoints: (builder) => ({
    getList: builder.query<IInfo<ILocation>, ILocationListRequest>({
      query: ({ page = 1, name = '' }) => `${RICK_AND_MORTY_API.LOCATIONS}?page=${page}${name ? `&name=${name}` : ''}`,
    }),
    getItem: builder.query<ILocation, number>({
      query: (locationId) => (typeof locationId === 'string' ? locationId : `${RICK_AND_MORTY_API.LOCATIONS}/${locationId}`),
    }),
  }),
});

export const { useGetListQuery: useLocationGetListQuery, useGetItemQuery: useLocationGetItemQuery } = locationApi;

export const { getList: locationGetList, getItem: locationGetItem } = locationApi.endpoints;
