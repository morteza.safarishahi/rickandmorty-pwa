import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/dist/query/react';
import API_BASE from '@/Module/Http/Constant/API';
import { IInfo } from '@/Module/RickAndMorty/Model/ApiModel/IInfo';
import { ICharacter } from '@/Module/RickAndMorty/Model/DataModel/ICharacter';
import RICK_AND_MORTY_API from '@/Module/RickAndMorty/Constant/RICK_AND_MORTY_API';
import { ICharacterListRequest } from '@/Module/RickAndMorty/Model/ApiModel/ICharacterListRequest';

export const charactersApi = createApi({
  reducerPath: 'charactersApi',
  baseQuery: fetchBaseQuery({ baseUrl: API_BASE }),
  endpoints: (builder) => ({
    getList: builder.query<IInfo<ICharacter>, ICharacterListRequest>({
      query: ({ page = 1, name = '', gender = null }) => `${RICK_AND_MORTY_API.CHARACTERS}?page=${page}${name ? `&name=${name}` : ''}${gender ? `&gender=${gender}` : ''}`,
    }),
    getItem: builder.query<ICharacter, number | string>({
      query: (characterId) => (typeof characterId === 'string' ? characterId : `${RICK_AND_MORTY_API.CHARACTERS}/${characterId}`),
    }),
  }),
});

export const { useGetListQuery: useCharacterGetListQuery, useGetItemQuery: useCharacterGetItemQuery } = charactersApi;
