import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/dist/query/react';
import API_BASE from '@/Module/Http/Constant/API';
import { IInfo } from '@/Module/RickAndMorty/Model/ApiModel/IInfo';
import RICK_AND_MORTY_API from '@/Module/RickAndMorty/Constant/RICK_AND_MORTY_API';
import { ILocation } from '@/Module/RickAndMorty/Model/DataModel/ILocation';

export const episodeApi = createApi({
  reducerPath: 'episodeApi',
  baseQuery: fetchBaseQuery({ baseUrl: API_BASE }),
  endpoints: (builder) => ({
    getList: builder.query<IInfo<ILocation>, number>({
      query: (page = 1) => `${RICK_AND_MORTY_API.EPISODES}?page=${page}`,
    }),
    getItem: builder.query<ILocation, number | string>({
      query: (episodeId) => (typeof episodeId === 'string' ? episodeId : `${RICK_AND_MORTY_API.EPISODES}/${episodeId}`),
    }),
  }),
});

export const { useGetListQuery: useEpisodeGetListQuery, useGetItemQuery: useEpisodeGetItemQuery } = episodeApi;
