import { IEntity } from '@/Module/RickAndMorty/Model/DataModel/IEntity';

export interface ILocation extends IEntity{
  type: string;
  dimension: string;
  residents: string[];
}
