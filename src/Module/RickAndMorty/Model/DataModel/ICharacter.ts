import CharacterStatus from '@/Module/RickAndMorty/Model/DataModel/CharacterStatus';
import CharacterGender from '@/Module/RickAndMorty/Model/DataModel/CharacterGender';
import { ICharacterLocation } from '@/Module/RickAndMorty/Model/DataModel/ICharacterLocation';
import { IEntity } from '@/Module/RickAndMorty/Model/DataModel/IEntity';

export interface ICharacter extends IEntity{
  status: CharacterStatus,
  species: string,
  type: string,
  gender: CharacterGender,
  origin: ICharacterLocation,
  location: ICharacterLocation,
  image: string,
  episode: string[],
}
