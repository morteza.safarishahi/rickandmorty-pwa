import { IEntity } from '@/Module/RickAndMorty/Model/DataModel/IEntity';

export interface IEpisode extends IEntity{
  type: string;
  air_date: string;
  episode: string;
  characters: string[];
}
