export interface ILocationListRequest {
  page: number;
  name?: string;
}
