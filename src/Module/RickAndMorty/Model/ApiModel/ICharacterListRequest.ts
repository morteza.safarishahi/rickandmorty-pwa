import CharacterGender from '@/Module/RickAndMorty/Model/DataModel/CharacterGender';

export interface ICharacterListRequest {
  page: number;
  name?: string;
  gender?: null | CharacterGender;
}
