import { FC } from 'react';
import { Card, List } from 'antd';
import { IEpisode } from '@/Module/RickAndMorty/Model/DataModel/IEpisode';
import s from './CharacterEpisodeItem.module.scss';

interface ILocationCharacterItemProps{
  episode: IEpisode;
}

const CharacterEpisodeItem: FC<ILocationCharacterItemProps> = ({ episode }) => (
  <List.Item>
    <Card className={s.container}>
      <div className={s.innerContainer}>
        <div className={s.titleContainer}>
          <span className={s.episode}>{episode.episode}</span>
          -
          <h3 className={s.title}>{episode.name}</h3>
        </div>
        <div className={s.air}>
          <span className={s.title}>
            Air Time:
          </span>
          <span className={s.value}>
            {episode.air_date}
          </span>
        </div>
      </div>
    </Card>
  </List.Item>
);

export default CharacterEpisodeItem;
