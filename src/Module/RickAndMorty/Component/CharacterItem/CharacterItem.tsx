import { FC } from 'react';
import Link from 'next/link';
import { Button, Card, Spin } from 'antd';
import { ICharacter } from '@/Module/RickAndMorty/Model/DataModel/ICharacter';
import LINKS from '@/Module/Application/Constant/LINKS';
import CharacterAliveStatus from '@/Module/RickAndMorty/Component/CharacterAliveStatus/CharacterAliveStatus';
import { useEpisodeGetItemQuery } from '@/Module/RickAndMorty/Store/episodeApi';
import { StarOutlined, StarFilled } from '@ant-design/icons';
import useAppSelector from '@/Module/Store/Hook/useAppSelector';
import { useRouter } from 'next/router';
import { IBaseComponentProps } from '@/Module/Application/Model/DataModel/IBaseComponentProps';
import cs from 'classnames';
import s from './CharacterItem.module.scss';

interface ICharacterItemProps extends IBaseComponentProps{
  item: ICharacter,
  showOrigin?: boolean;
}

const CharacterItem: FC<ICharacterItemProps> = ({ item, className, showOrigin }) => {
  const router = useRouter();
  const favoriteId = useAppSelector((state) => state.user.favoriteId);

  const linkHref = {
    pathname: LINKS.CHARACTER,
    query: {
      id: item.id,
    },
  };

  const { isLoading: episodeIsLoading, data: episode } = useEpisodeGetItemQuery(item?.episode?.[0] || '');

  return (
    (
      <Card className={cs(s.item, className)}>
        <div className={s.container}>
          <div className={s.imageContainer}>
            <Link href={linkHref}>
              <a>
                <img
                  src={item.image}
                  alt={item.name}
                  loading="lazy"
                />
              </a>
            </Link>

            <Button
              type="primary"
              title="set favorite"
              icon={favoriteId === item.id ? <StarFilled /> : <StarOutlined />}
              className={s.favButton}
              shape="circle"
              onClick={(event) => {
                event.preventDefault();
                router.replace({
                  pathname: LINKS.FAVORITE,
                  query: {
                    id: `character-${item.id}`,
                    redirect: encodeURIComponent(document.location.pathname),
                  },
                });
              }}
            />
          </div>
          <div className={s.contentContainer}>
            <div className={s.titleContainer}>
              <Link href={linkHref}>
                <a><h2 className={s.title}>{item.name}</h2></a>
              </Link>
            </div>
            <div className={s.infoContainer}>
              <CharacterAliveStatus
                status={item.status}
                className={s.infoItem}
              />
              <div className={s.infoItem}>
                {item.species}
              </div>
            </div>
            <div className={s.detailContainer}>
              <div className={s.detailItem}>
                <span className={s.detailTitle}>Last known location:</span>
                <span className={s.detailValue}>{item.location.name}</span>
              </div>
              <div className={s.detailItem}>
                <span className={s.detailTitle}>First seen in:</span>
                <span className={s.detailValue}>{episodeIsLoading ? <Spin size="small" /> : episode?.name}</span>
              </div>
              {
                  showOrigin && (
                    <div className={s.detailItem}>
                      <span className={s.detailTitle}>Origin:</span>
                      <span className={s.detailValue}>{item.origin.name}</span>
                    </div>
                  )
                }
            </div>
          </div>
        </div>
      </Card>
    )
  );
};

CharacterItem.defaultProps = {
  showOrigin: false,
};

export default CharacterItem;
