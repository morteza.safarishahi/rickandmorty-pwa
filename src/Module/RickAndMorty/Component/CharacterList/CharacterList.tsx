import { ChangeEventHandler, FC, useRef, useState } from 'react';
import { Input, List, Select, Spin } from 'antd';
import { useCharacterGetListQuery } from '@/Module/RickAndMorty/Store/characterApi';
import { ICharacter } from '@/Module/RickAndMorty/Model/DataModel/ICharacter';
import CharacterItem from '@/Module/RickAndMorty/Component/CharacterItem/CharacterItem';
import CharacterGender from '@/Module/RickAndMorty/Model/DataModel/CharacterGender';
import s from './CharacterList.module.scss';

const GENDER_OPTIONS = [
  {
    label: 'Male',
    value: CharacterGender.Male,
  },
  {
    label: 'Female',
    value: CharacterGender.Female,
  },
  {
    label: 'Genderless',
    value: CharacterGender.Genderless,
  },
  {
    label: 'unknown',
    value: CharacterGender.unknown,
  },
];

const CharacterList: FC = () => {
  const headerRef = useRef<HTMLDivElement>(null);

  const [name, setName] = useState('');
  const [gender, setGender] = useState<null | CharacterGender>(null);
  const [page, setPage] = useState(1);

  const { isLoading, data, isError } = useCharacterGetListQuery({
    page,
    name,
    gender,
  });

  const onNameChange:ChangeEventHandler<HTMLInputElement> = (event) => {
    setName(event.target.value);
    setPage(1);
  };

  const onGenderChange = (value: null | CharacterGender) => {
    setPage(1);
    setGender(value);
  };

  const onPageChange = (page: number) => {
    setPage(page);
    headerRef.current?.scrollIntoView({
      behavior: 'smooth',
    });
  };

  return (
    <div className={s.container}>
      <div
        className={s.header}
        ref={headerRef}
      >
        <h1 className={s.title}>
          <Spin
            className="m-r-8"
            spinning={isLoading}
          />
          Character List
        </h1>

        <div className={s.filters}>
          <Input
            placeholder="enter name for search"
            title="Name:"
            onChange={onNameChange}
            value={name}
            allowClear
          />
          <Select
            placeholder="gender"
            allowClear
            options={GENDER_OPTIONS}
            onChange={onGenderChange}
          />
        </div>
      </div>

      <List<ICharacter>
        grid={{
          gutter: 16,
          xs: 1,
          sm: 2,
          md: 2,
          xxl: 4,
          column: 3,
        }}
        pagination={{
          current: page,
          pageSize: 20,
          showSizeChanger: false,
          total: data?.info?.count || 0,
          onChange: onPageChange,
          disabled: isLoading,
          hideOnSinglePage: true,
        }}
        loading={isLoading}
        rowKey="id"
        dataSource={isError ? [] : data?.results}
        renderItem={(item) => (
          <List.Item key={item.id}>
            <CharacterItem item={item} />
          </List.Item>
        )}
      />
    </div>
  );
};

export default CharacterList;
