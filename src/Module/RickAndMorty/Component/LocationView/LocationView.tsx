import { FC } from 'react';
import useAppSelector from '@/Module/Store/Hook/useAppSelector';
import { locationSelectById } from '@/Module/RickAndMorty/Store/locationSlice';
import LocationCharacterList from '@/Module/RickAndMorty/Component/LocationCharacterList/LocationCharacterList';
import s from './LocationView.module.scss';

interface ILocationViewProps{
  locationId: number;
}

const LocationView: FC<ILocationViewProps> = ({ locationId }) => {
  const location = useAppSelector((state) => locationSelectById(state, locationId));

  if (!location) return null;

  return (
    <div className={s.container}>
      <div className={s.header}>
        <div className={s.titleContainer}>
          <span className={s.id}>
            #
            {locationId}
          </span>
          -
          <h1 className={s.title}>{location.name}</h1>
        </div>

        <div className={s.infoContainer}>
          <div className={s.type}>
            type:
            {location.type}
          </div>
          <div className={s.type}>
            Last known location: :
            {location.dimension}
          </div>
        </div>
      </div>

      <LocationCharacterList characterUrls={location.residents} />
    </div>
  );
};

export default LocationView;
