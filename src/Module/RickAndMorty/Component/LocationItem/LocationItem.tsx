import { FC } from 'react';
import Link from 'next/link';
import { Card } from 'antd';
import LINKS from '@/Module/Application/Constant/LINKS';
import { ILocation } from '@/Module/RickAndMorty/Model/DataModel/ILocation';
import { IBaseComponentProps } from '@/Module/Application/Model/DataModel/IBaseComponentProps';
import cs from 'classnames';
import s from './LocationItem.module.scss';

interface ILocationItemProps extends IBaseComponentProps{
  item: ILocation,
  extraTitle?: string;
}

const LocationItem: FC<ILocationItemProps> = ({ item, className, extraTitle }) => {
  const linkHref = {
    pathname: LINKS.LOCATION,
    query: {
      id: item.id,
    },
  };

  return (
    (
      <Card className={cs(s.item, className)}>
        <div className={s.container}>
          <div className={s.titleContainer}>
            <Link href={linkHref}>
              <a><h2 className={s.title}>{item.name}</h2></a>
            </Link>

          </div>
          <div className={s.infoContainer}>
            <div className={s.infoItem}>
              {item.type}
            </div>
          </div>
          <div className={s.detailContainer}>
            <div className={s.detailItem}>
              <span className={s.detailTitle}>Dimension:</span>
              <span className={s.detailValue}>{item.dimension}</span>
            </div>
          </div>
          {extraTitle && (
          <div className={s.extraTitle}>
            <span>{extraTitle}</span>
          </div>
          ) }
        </div>
      </Card>
    )
  );
};

LocationItem.defaultProps = {
  extraTitle: '',
};

export default LocationItem;
