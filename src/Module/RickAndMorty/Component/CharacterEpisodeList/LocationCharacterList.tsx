import { FC, useMemo } from 'react';
import { List } from 'antd';
import { IEpisode } from '@/Module/RickAndMorty/Model/DataModel/IEpisode';
import CharacterEpisodeItem from '@/Module/RickAndMorty/Component/CharacterEpisodeItem/CharacterEpisodeItem';
import s from './LocationCharacterList.module.scss';

interface ILocationCharacterList {
  episodes: IEpisode[]
}

const LocationCharacterList: FC<ILocationCharacterList> = ({ episodes }) => {
  const sortedEpisodes = useMemo(() => episodes.sort((a, b) => a.id - b.id), [episodes]);

  return (
    <div className={s.container}>
      <div className={s.header}>
        <h1 className={s.title}>
          Episode List
        </h1>
      </div>

      <List
        grid={{
          column: 1,
        }}
        dataSource={sortedEpisodes}
        renderItem={(item) => (
          <CharacterEpisodeItem
            key={item.id}
            episode={item}
          />
        )}
      />
    </div>
  );
};

export default LocationCharacterList;
