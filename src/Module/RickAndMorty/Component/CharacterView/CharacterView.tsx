import { FC } from 'react';
import { ICharacter } from '@/Module/RickAndMorty/Model/DataModel/ICharacter';
import CharacterItem from '@/Module/RickAndMorty/Component/CharacterItem/CharacterItem';
import { ILocation } from '@/Module/RickAndMorty/Model/DataModel/ILocation';
import LocationItem from '@/Module/RickAndMorty/Component/LocationItem/LocationItem';
import LocationCharacterList from '@/Module/RickAndMorty/Component/CharacterEpisodeList/LocationCharacterList';
import { IEpisode } from '@/Module/RickAndMorty/Model/DataModel/IEpisode';
import s from './CharacterView.module.scss';

interface ICharacterViewProps{
  character: ICharacter;
  episodes: IEpisode[];
  origin?: ILocation;
  location?: ILocation;
}

const CharacterView: FC<ICharacterViewProps> = ({ character, episodes, origin, location }) => (
  <div className={s.container}>
    <div className={s.row}>
      <CharacterItem
        item={character}
        className={s.detail}
        showOrigin
      />

      {location && (
        <LocationItem
          item={location}
          extraTitle="Location"
          className={s.location}
        />
      )}

      {origin && (
        <LocationItem
          item={origin}
          extraTitle="Origin"
          className={s.origin}
        />
      )}
    </div>

    <LocationCharacterList episodes={episodes} />
  </div>
);

CharacterView.defaultProps = {
  origin: undefined,
  location: undefined,
};

export default CharacterView;
