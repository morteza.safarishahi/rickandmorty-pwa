import { ChangeEventHandler, FC, useRef, useState } from 'react';
import { Input, List, Spin } from 'antd';
import { useLocationGetListQuery } from '@/Module/RickAndMorty/Store/locationApi';
import { ILocation } from '@/Module/RickAndMorty/Model/DataModel/ILocation';
import LocationItem from '@/Module/RickAndMorty/Component/LocationItem/LocationItem';
import s from './LocationList.module.scss';

const LocationList: FC = () => {
  const headerRef = useRef<HTMLDivElement>(null);

  const [name, setName] = useState('');
  const [page, setPage] = useState(1);

  const { isLoading, data, isError } = useLocationGetListQuery({
    page,
    name,
  });

  const onNameChange:ChangeEventHandler<HTMLInputElement> = (event) => {
    setPage(1);
    setName(event.target.value);
  };

  const onPageChange = (page: number) => {
    setPage(page);
    headerRef.current?.scrollIntoView({
      behavior: 'smooth',
    });
  };

  return (
    <div className={s.container}>
      <div
        className={s.header}
        ref={headerRef}
      >
        <h1 className={s.title}>
          <Spin
            className="m-r-8"
            spinning={isLoading}
          />
          Location List
        </h1>

        <div className={s.filters}>
          <Input
            placeholder="enter name for search"
            title="Name:"
            onChange={onNameChange}
            value={name}
            allowClear
          />
        </div>
      </div>

      <List<ILocation>
        grid={{
          gutter: 16,
          xs: 1,
          sm: 2,
          xxl: 4,
          column: 3,
        }}
        pagination={{
          current: page,
          pageSize: 20,
          showSizeChanger: false,
          total: data?.info?.count || 0,
          onChange: onPageChange,
          disabled: isLoading,
          hideOnSinglePage: true,
        }}
        loading={isLoading}
        rowKey="id"
        dataSource={isError ? [] : data?.results}
        renderItem={(item) => (
          <List.Item key={item.id}>
            <LocationItem
              item={item}
            />
          </List.Item>
        )}
      />
    </div>
  );
};

export default LocationList;
