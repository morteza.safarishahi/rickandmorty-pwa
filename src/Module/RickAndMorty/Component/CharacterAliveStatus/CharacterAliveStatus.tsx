import { FC } from 'react';
import cs from 'classnames';
import { IBaseComponentProps } from '@/Module/Application/Model/DataModel/IBaseComponentProps';
import CharacterStatus from '@/Module/RickAndMorty/Model/DataModel/CharacterStatus';
import s from './CharacterAliveStatus.module.scss';

interface ICharacterAliveStatusProps extends IBaseComponentProps{
  status: CharacterStatus
}

const CharacterAliveStatus: FC<ICharacterAliveStatusProps> = ({ status, className }) => (
  <span className={cs(s.container, className)}>
    <span className={cs(s.dot, s[`status-${status}`])} />
    {status}
  </span>
);

export default CharacterAliveStatus;
