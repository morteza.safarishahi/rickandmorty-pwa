import { FC } from 'react';
import { Card, List, Skeleton } from 'antd';
import { useCharacterGetItemQuery } from '@/Module/RickAndMorty/Store/characterApi';
import Link from 'next/link';
import LINKS from '@/Module/Application/Constant/LINKS';
import cs from 'classnames';
import s from './LocationCharacterItem.module.scss';

interface ILocationCharacterItemProps{
  url: string;
}

const LocationCharacterLoading: FC = () => (
  <div className={cs(s.container, s.loading)}>
    <div className={s.imageContainer} />
    <div className={s.titleContainer}>
      <Skeleton
        paragraph={false}
        active
      />
    </div>
  </div>
);

const LocationCharacterItem: FC<ILocationCharacterItemProps> = ({ url }) => {
  const { isLoading, data: character } = useCharacterGetItemQuery(url);

  return (
    <List.Item>
      <Card
        className={s.container}
      >
        {
          isLoading ? <LocationCharacterLoading /> : (
            <Link href={{
              pathname: LINKS.CHARACTER,
              query: {
                id: character?.id || 0,
              },
            }}
            >
              <a>
                <div className={s.imageContainer}>

                  {
                  character && (
                    <img
                      src={character.image}
                      alt={character.name}
                      loading="lazy"
                    />
                  )
                }

                </div>
                <div className={s.titleContainer}>
                  <h2 className={s.title}>{character?.name}</h2>
                </div>
              </a>
            </Link>
          )
        }
      </Card>
    </List.Item>
  );
};

export default LocationCharacterItem;
