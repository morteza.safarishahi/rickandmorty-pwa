import { FC } from 'react';
import { List } from 'antd';
import LocationCharacterItem from '@/Module/RickAndMorty/Component/LocationCharacterItem/LocationCharacterItem';
import s from './LocationCharacterList.module.scss';

interface ILocationCharacterList {
  characterUrls: string[]
}

const LocationCharacterList: FC<ILocationCharacterList> = ({ characterUrls }) => (
  <div className={s.container}>
    <div
      className={s.header}
    >
      <h1 className={s.title}>
        Residents/Character List
      </h1>
    </div>

    <List
      grid={{
        gutter: 16,
        xs: 2,
        sm: 3,
        xxl: 5,
        column: 4,
      }}
      dataSource={characterUrls}
      renderItem={(item) => (
        <LocationCharacterItem
          url={item}
          key={item}
        />
      )}
    />
  </div>
);

export default LocationCharacterList;
