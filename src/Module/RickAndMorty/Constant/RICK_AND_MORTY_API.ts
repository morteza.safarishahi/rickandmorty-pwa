const RICK_AND_MORTY_API = {
  CHARACTERS: '/character',
  LOCATIONS: '/location',
  EPISODES: '/episodes',
};

export default RICK_AND_MORTY_API;
